$(document).ready(function(){

    $(".nav-mobile").click(function() {
        var heightView = $(document).height();
        $(".menu-mobile").css("height", heightView + "px").slideToggle();
    }); 

	$(".menu-main > a").click(function(){
		$(this).siblings().slideToggle();
	});	

	(function(){
		$(".view-content-main > div").clone().appendTo(".view-mobile");
	}());
	
});